import { MazeService } from './maze.service';
import { TestBed } from '@angular/core/testing';
import { Maze } from '../models';

describe('MazeService', () => {
  let service: MazeService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MazeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should return value from observable of type Maze', () => {
    service.mazeSubject$.subscribe((maze) => {
      expect(maze).toEqual(jasmine.any(Maze));
    });
  });
});
