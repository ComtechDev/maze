import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Maze, MazeCell, MazeRow } from '../models/index';

@Injectable({
  providedIn: 'root',
})
export class MazeService {
  public mazeBadMoves$: BehaviorSubject<number>;
  public mazeSubject$: BehaviorSubject<Maze>;
  public mazeComplete$: BehaviorSubject<boolean>;

  first = true;
  private newMaze = new Maze();
  private badMoves = 0;
  mazeA = [
    [0, 1, 1, 1, 1],
    [0, 0, 1, 1, 1],
    [1, 0, 1, 1, 1],
    [1, 0, 0, 1, 1],
    [1, 1, 0, 1, 1],
    [1, 0, 0, 1, 1],
    [1, 0, 1, 1, 1],
  ];

  mazeB = [
    [1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1],
    [1, 0, 0, 0, 1],
    [1, 1, 0, 1, 1],
    [1, 1, 0, 0, 1],
    [1, 1, 1, 0, 0],
    [1, 1, 1, 1, 0],
  ];

  constructor() {
    this.mazeBadMoves$ = new BehaviorSubject<number>(0);
    this.mazeSubject$ = new BehaviorSubject<Maze>(null);
    this.mazeComplete$ = new BehaviorSubject<boolean>(false);
    this.initialiseMaze();
  }

  private initialiseMaze(): void {
    this.newMaze.mazeRowCollection = [];
    const activeMaze = Math.random() > 0.6 ? this.mazeA : this.mazeB;
    for (let index = 0; index < activeMaze.length; index++) {
      const element = activeMaze[index];

      let mr = new MazeRow();
      const cells: MazeCell[] = [];

      for (let cellIndex = 0; cellIndex < element.length; cellIndex++) {
        const cellElement = element[cellIndex];
        const mc = new MazeCell();

        if (this.first && cellElement === 0) {
          mc.reveled = true;
          this.first = false;
        } else {
          mc.reveled = false;
        }
        mc.solid = cellElement === 1 ? true : false;
        cells.push(mc);
      }
      mr.mazeRow = cells;
      this.newMaze.mazeRowCollection.push(mr);
    }

    this.mazeSubject$.next(this.newMaze);
  }

  mazeEvent(row: number, cell: number): void {
    if (this.checkReveal(row, cell)) {
      let mr = this.newMaze.mazeRowCollection[row];
      let mc = mr.mazeRow[cell];
      mc.reveled = true;

      this.mazeSubject$.next(this.newMaze);

      if (mc.solid) {
        this.mazeBadMoves$.next(++this.badMoves);
      }
      this.checkComplete(row, cell);
    }
  }

  private checkComplete(row: number, cell: number): void {
    if (row === this.newMaze.mazeRowCollection.length - 1) {
      const mazeCell = this.newMaze.mazeRowCollection[row].mazeRow[cell];
      if (!mazeCell.solid) {
        this.mazeComplete$.next(true);
      }
    }
  }

  private checkReveal(row: number, cell: number): boolean {
    if (
      row < 0 ||
      row >= this.newMaze.mazeRowCollection.length ||
      cell < 0 ||
      cell >= this.newMaze.mazeRowCollection[0].mazeRow.length
    ) {
      return false;
    }

    //North
    if (row > 0) {
      const northMazeCell = this.newMaze.mazeRowCollection[row - 1].mazeRow[
        cell
      ];
      if (northMazeCell.reveled && !northMazeCell.solid) {
        return true;
      }
    }

    //South
    if (row < this.newMaze.mazeRowCollection.length - 1) {
      const southMazeCell = this.newMaze.mazeRowCollection[row + 1].mazeRow[
        cell
      ];
      if (southMazeCell.reveled && !southMazeCell.solid) {
        return true;
      }
    }

    //West
    if (cell > 0) {
      const westMazeCell = this.newMaze.mazeRowCollection[row].mazeRow[
        cell - 1
      ];
      if (westMazeCell.reveled && !westMazeCell.solid) {
        return true;
      }
    }

    //East
    if (cell < this.newMaze.mazeRowCollection[0].mazeRow.length - 1) {
      const eastMazeCell = this.newMaze.mazeRowCollection[row].mazeRow[
        cell + 1
      ];
      if (eastMazeCell.reveled && !eastMazeCell.solid) {
        return true;
      }
    }
    return false;
  }
}
