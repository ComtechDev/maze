import { Component, OnInit } from '@angular/core';
import { Maze } from '../models';
import { MazeService } from '../service/index';

@Component({
  selector: 'app-maze',
  templateUrl: './maze.component.html',
  styleUrls: ['./maze.component.css'],
})
export class MazeComponent implements OnInit {
  maze: Maze;
  badMoves: number;
  complete = false;
  gaveUp = false;

  constructor(private mazeService: MazeService) {}

  ngOnInit(): void {
    this.setUpObservables();
  }

  private setUpObservables(): void {
    this.mazeService.mazeSubject$.subscribe((data) => {
      this.maze = data;
    });

    this.mazeService.mazeBadMoves$.subscribe((moves) => {
      this.badMoves = moves;
    });

    this.mazeService.mazeComplete$.subscribe((status) => {
      this.complete = status;
    });
  }

  reveal(row: number, cell: number): void {
    console.log(row, cell);
    this.mazeService.mazeEvent(row, cell);
  }

  iGiveUp(): void {
    this.gaveUp = !this.gaveUp;
    this.complete = true;
  }
}
