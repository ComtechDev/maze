import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { MazeComponent } from './maze.component';

describe('MazeComponent', () => {
  let component: MazeComponent;
  let fixture: ComponentFixture<MazeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MazeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MazeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show Solve Me Button', () => {
    const buttons = fixture.debugElement.queryAll(By.css('.btn'));
    expect(buttons[0].nativeElement.textContent).toEqual('Solve Me!');
  });

  it('should show Bad Moves so far: 0', () => {
    const badMove = fixture.debugElement.queryAll(By.css('#control'));
    console.dir(badMove);
    expect(badMove).toBeTruthy();
  });

  it('should show maze as a table', () => {
    const table = fixture.debugElement.queryAll(By.css('.table'));
    expect(table.length).toBe(1);
  });

  it('should show table with 7 rows', () => {
    const tableRows = fixture.debugElement.queryAll(By.css('table tr'));
    expect(tableRows.length).toBe(7);
  });

  it('should show table with 35 cells', () => {
    const tableCells = fixture.debugElement.queryAll(By.css('table tr td'));
    expect(tableCells.length).toBe(35);
  });

  it('should show maze as a table with one revealed square', () => {
    const tableCells = fixture.debugElement.queryAll(
      By.css('table tr td.cell.hide')
    );
    expect(tableCells.length).toBe(34);
  });

  it('should show maze as a table with one revealed square', () => {
    const tableCells = fixture.debugElement.queryAll(
      By.css('table tr td.cell.hide')
    );
    expect(tableCells.length).toBe(34);
  });

  it('should reveal the maze when the Solve me button is clicked', () => {
    component.iGiveUp();
    fixture.detectChanges();
    const tableCells = fixture.debugElement.queryAll(
      By.css('table tr td.cell.hide')
    );
    expect(tableCells.length).toBe(0);
  });
});
