# Maze

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Getting started
I use VS Code to develop angular apps and in order to run the app you will need to follow thes steps:-

1. Machine will require Node.JS installed 
2. Open the source code in VS Code \Src\ folder
3. Run npm Install -  This will pull the node packages needed
4. run `ng serve`
5. Open a browser (Edge or chrome as you need Javascript) address above

## Notes
This is a basic maze game, the main code structure is a page with a small service that keeps track of the users interaction
I have also included some tests to cover the code

## Improvments
There are a few things that i would improve if time allowed 
Add in random maze generation (large work piece)
Add in local storage to the users highscore results
Add a timer to make it more intresting
Refactor the service to make testing easier


Ron Prowse
